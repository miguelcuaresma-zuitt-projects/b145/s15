// console.log() is used to output/display messages in the console of the browser
console.log("Hello JS");

// There are multiple ways to display messages in the console using different formats.

// error message
console.error("Something went wrong");

//warning message
console.warn("1st warning");

// syntax:
/*
	function nameOfFunction() {
	instruction/procedures
	return
	}
*/

function errorMessage(){
	console.error("Oh nose!!")
}
errorMessage();

function greetings() {
	console.log("Salutations from Javascript")
};

// Different methods in declaring a function in JS

// [SECTION 1] variables declared outside a function can be used inside a function



function fullName() {
	// combine the values of the info outside and display it inside the console.
	console.log(givenName + familyName);
};
let givenName = "John";
let familyName = "Doe";
fullName();

const es6 = () => console.log("You just made an ES6 function");
es6();

//[SECTION 2] "blocked scope" variables, are variables that can only be used within the scope of the function.

function computeTotal() {
	let numA = 20;
	let numB = 5;
	// add the values and display it inside the console.
	console.log(numA + numB);
}

computeTotal();

// [SECTION 3] Functions with Parameters

// "Parameter" -> acts as a variable or a container/catchers that only exists inside a function. A parameter is used to store information that is provided to a function when it is called or invoked.
// basically a placeholder
// lets create a function that emulate a pokemon battle

function pokemon(pangalan) {
	// were going to use the "parameter" declared on this function to be processed and display inside the console.
	console.log("I choose you: " + pangalan)
};

pokemon("Picakechu")

// parameters VS arguments
// Argument -> is the ACTUAL value that is provided inside a function for it to work properly. The TERM argument is used when functions are called/invoked as compared to parameters when a function is declared.

// [SECTON 4] Functions with multiple parameters

// lets declare a function that will get the sum of multiple values.

function addNumbers(numA, NumB, numC, numD, numE) {
	console.log(numA + NumB +numC + numD + numE);
};

addNumbers(1,2,3,4,5);

function createFullName(firstName, middleName, lastName) {
	console.log(lastName + ", " + firstName + " " + middleName);
}
createFullName("Miguel Alvaro", "Gabriel", "Cuaresma");

// [SECTION 5] using variables as arguments

// lets create a function that will display the stats of a pokemon in battle.

let attackA = "Tackle";
let attackB = "Thunderbolt";
let attackC = "Quick Attack";
let selectPokemon = "Ratata";

function pokemonStats(name, attack) {
	console.log(name + " use " + attack)
}
pokemonStats(selectPokemon, attackC);
// Ratta use Quick Attack

// Note: using variables as arguments will allow us to utilize code reusabilty, this will help us reduce the amount of code that we have to write.

// Use of "return" expression/statement 
// the return statement

// [SECTION 6] The RETURN statement
// allows out of the function to be passed to the line/block of code that called the function
// create a function that will return a string message.
function returnString(){
	"Hello World"
	return 2 + 1;
}

// lets repackage the result of this function inside a new variable
let functionOutput = returnString();
console.log(functionOutput);

// lets create a simple function to demonstrate the use and behavior of the return statement again.
function dialog(){
	console.log("Ooops! I did it Again")
	return ("Don't you know that you're toxic")
	console.log("I'm a slave for you")
	console.log("I will be ignored")
};

// note: any block or line of code that will come after "return" statement will be ignored because it came after the end of the function execution.
// the main purpose of the "return" statement is to identify which will be the final output/result of the function and at the same time determine the end of the function statement.

console.log(dialog());

// [SECTION 7] Using functions as arguments
// function parameters can also accept other functions as their argument
// some complex functions use other functions as their arguments to perform more complicated/complex results

function argumentSaFunction(){
	console.log("This function was passed as an argument");

};

function invokeFunction(argumentNaFunction, pangalawangFunction) {
	argumentNaFunction();
	pangalawangFunction(selectPokemon, attackC);
	pangalawangFunction(selectPokemon, attackA);
};

invokeFunction(argumentSaFunction,pokemonStats);
console.log(invokeFunction)